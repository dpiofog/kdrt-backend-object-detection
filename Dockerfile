#syntax=docker/dockerfile:1
FROM python:3.7

WORKDIR /gitlab_object_detection

RUN apt-get update && apt-get install -y python3-opencv

RUN pip install imageai

RUN pip install tensorflow==2.4.0

RUN pip install keras==2.4.3

RUN pip install numpy==1.19.3

RUN pip install pillow==7.0.0

RUN pip install scipy==1.4.1

RUN pip install h5py==2.10.0

RUN pip install matplotlib==3.3.2

RUN pip install keras-resnet==0.2.0

RUN pip install opencv-python

RUN pip install  flask

RUN pip install werkzeug

RUN pip install flask-restx

EXPOSE 5000

COPY . .

CMD [ "python3", "-u", "-m", "flask", "run", "--host=0.0.0.0"]

