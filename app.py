from flask import Flask, Response, jsonify, redirect, url_for, request, render_template, send_from_directory
from flask.helpers import flash
from flask_restx import Api, Resource
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
import os
from imageai.Detection import VideoObjectDetection
import json





app = Flask(__name__)
app.config['UPLOAD_PATH'] = '/input'

# Set maximum size file 
app.config['MAX_CONTENT_LENGTH'] = 75 * 1024 * 1024
# Allowed extension
ALLOWED_EXTENSIONS = {'mp4', 'mov', 'wmv', 'avi', 'mkv'}

api = Api(app, version = "1.0.1", 
        title= "Angelica Backend Object Detection Documentation",
        description = "Upload video to be processed and identify",
         )



upload_parser = api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)

execution_path = os.getcwd()
detector = VideoObjectDetection()
average_detected_object = {}

detector.setModelTypeAsTinyYOLOv3()
detector.setModelPath(os.path.join(execution_path, "models", "yolo-tiny.h5"))
detector.loadModel(detection_speed="fastest")



ai_name_space = api.namespace('api-detection', descriptions='Manage API for Object Detection AI')


@api.route('/predict')
@api.expect(upload_parser)
class Predict(Resource):
    def post(self):
        print("Saving file")
        args = upload_parser.parse_args()
        file = args['file']
        file_name = secure_filename(file.filename)
        basepath = os.path.dirname(__file__)
        file.save(os.path.join(
            basepath,'input',file_name
        ))
        

        
        try:
            # Make predictions
            print("Detecting")
            results = getDetections(file_name)
            print("Completed")
            os.remove("input/{file_name}".format(file_name=file_name))
            return {
                "Status" : "Success",
                "statusCode" : "200",
                "result" :average_detected_object
            }
        
        except Exception as e :
            ai_name_space.abort(400, status = " Could not retrieve information", statusCode = '400')
            return

        
        

      


def forFull(output_arrays, count_arrays, average_output_count):
    print("Output average count for unique objects in the last second: ", average_output_count)
    global average_detected_object
    average_detected_object.clear()
    average_detected_object = average_output_count

# def forFull(output_arrays, count_arrays, average_output_count):
#     print("Output average count for unique objects in the last second: ", average_output_count)
#     print("------------END OF THE VIDEO --------------")
    


def getDetections(file_name):
    input_path = "./input/"
    output_path = "./output/"

    # global detector
    custom_objects = detector.CustomObjects(person=True, backpack=True, umbrella=True,handbag=True,tie=True,suitcase=True, sports_ball=True, baseball_bat=True,baseball_glove=True
    ,bottle=True,  cup=True,fork=True, knife=True, spoon=True, bowl=True,chair=True,potted_plant=True,laptop=True,mouse=True,keyboard=True, cell_phone=True,book=True,vase=True,scissors=True )

    
    # detections = detector.detectObjectsFromVideo(input_file_path=input_path+file_name,
    #                             output_file_path=output_path+file_name
    #                             , frames_per_second=20, log_progress=True, video_complete_function=forFull, return_detected_frame= True, minimum_percentage_probability=30)

    detections = detector.detectCustomObjectsFromVideo(
        custom_objects=custom_objects,
        input_file_path=input_path+file_name,
        # output_file_path= output_path+file_name+"_detected",
        save_detected_video = False,
        # frames_per_second=30,
        log_progress=True,
        minimum_percentage_probability=30,
        video_complete_function=forFull,
        return_detected_frame= True,
        thread_safe=True,
        )

    return detections
    

    
    

if __name__ == '__main__':
    app.run(host="0.0.0.0",threaded=True, port=5000) # we can use the threading to detect